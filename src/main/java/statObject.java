import java.util.*;

public class statObject {
    public HashMap<String, Float> numericStats;
    public HashMap<String, Integer> wordCountStats;
    public String sourceFile;
    public statObject(){
        this.numericStats = null;
        this.wordCountStats = null;
        this.sourceFile = null;
    }
    public statObject(HashMap<String, Float> numericStats, HashMap<String, Integer> wordCountStats, String sourceFile){
        this.numericStats = numericStats;
        this.wordCountStats = wordCountStats;
        this.sourceFile = sourceFile;
    }
    /**
     * @param numericStats the numericStats to set
     */
    public void setNumericStats(HashMap<String, Float> numericStats) {
        this.numericStats = numericStats;
    }
    /**
     * @param wordCountStats the wordCountStats to set
     */
    public void setWordCountStats(HashMap<String, Integer> wordCountStats) {
        this.wordCountStats = wordCountStats;
    }
    /**
     * @param sourceFile the sourceFile to set
     */
    public void setSourceFile(String sourceFile) {
        this.sourceFile = sourceFile;
    }
    /**
     * @return the numericStats
     */
    public HashMap<String, Float> getNumericStats() {
        return numericStats;
    }
    /**
     * @return the wordCountStats
     */
    public HashMap<String, Integer> getWordCountStats() {
        return wordCountStats;
    }
    /**
     * @return the sourceFile
     */
    public String getSourceFile() {
        return sourceFile;
    }
}