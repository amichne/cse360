import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

class TextPanel extends JPanel implements ActionListener {

	JButton browseButton;
	JTextArea textArea;
	JFileChooser fileChooser;
	ArrayList<statObject> results = new ArrayList<statObject>();

	JLabel lineCountLabelResult = new JLabel("");
	JLabel blankLineCountLabelResult = new JLabel("");
	JLabel spaceCountLabelResult = new JLabel("");
	JLabel wordCountLabelResult = new JLabel("");
	JLabel charCountLabelResult = new JLabel("");
	JLabel averageCharacterLineLabelResult = new JLabel("");
	JLabel averageWordLengthLabelResult = new JLabel("");
	JLabel maxWordOccurResult = new JLabel("");

	JLabel lineCountLabel = new JLabel("Line Count:");
	JLabel blankLineCountLabel = new JLabel("Blank Line Count:");
	JLabel spaceCountLabel = new JLabel("Space Count:");
	JLabel wordCountLabel = new JLabel("Word Count:");
	JLabel charCountLabel = new JLabel("Character Count:");
	JLabel averageCharacterLineLabel = new JLabel("Average Character Line:");
	JLabel averageWordLengthLabel = new JLabel("Average Word Length:");
	JLabel maxWordOccur = new JLabel("Max Word Occurence:");

	JLabel avg_lineCountLabelResult = new JLabel("");
	JLabel avg_blankLineCountLabelResult = new JLabel("");
	JLabel avg_spaceCountLabelResult = new JLabel("");
	JLabel avg_wordCountLabelResult = new JLabel("");
	JLabel avg_charCountLabelResult = new JLabel("");
	JLabel avg_averageCharacterLineLabelResult = new JLabel("");
	JLabel avg_averageWordLengthLabelResult = new JLabel("");
	JLabel avg_file_count = new JLabel("");
	JLabel avg_lineCountLabel = new JLabel("Average Line Count:");
	JLabel avg_blankLineCountLabel = new JLabel("Average Blank Line Count:");
	JLabel avg_spaceCountLabel = new JLabel("Average Space Count:");
	JLabel avg_wordCountLabel = new JLabel("Average Word Count:");
	JLabel avg_charCountLabel = new JLabel("Average Character Count:");
	JLabel avg_averageCharacterLineLabel = new JLabel("Average Character Line across files:");
	JLabel avg_averageWordLengthLabel = new JLabel("Average Word Length across files:");
	JLabel file_count = new JLabel("Number of files:");

	JPanel buttonPanel = new JPanel();

	public TextPanel() {
		super(new BorderLayout());

		fileChooser = new JFileChooser();
		browseButton = new JButton("Open a File...");
		browseButton.addActionListener(this);

		buttonPanel.add(browseButton);
		add(buttonPanel, BorderLayout.PAGE_START);

		GridLayout experimentLayout = new GridLayout(8,1);
		JPanel expPanel = new JPanel();
		expPanel.setLayout(experimentLayout);

		expPanel.add(lineCountLabel);
		expPanel.add(lineCountLabelResult);		
		expPanel.add(blankLineCountLabel);
		expPanel.add(blankLineCountLabelResult);
		expPanel.add(spaceCountLabel);
		expPanel.add(spaceCountLabelResult);
		expPanel.add(wordCountLabel);
		expPanel.add(wordCountLabelResult);
		expPanel.add(charCountLabel);
		expPanel.add(charCountLabelResult);
		expPanel.add(averageCharacterLineLabel);
		expPanel.add(averageCharacterLineLabelResult);
		expPanel.add(averageWordLengthLabel);
		expPanel.add(averageWordLengthLabelResult);
		expPanel.add(maxWordOccur);
		expPanel.add(maxWordOccurResult);
		add(expPanel, BorderLayout.WEST);
		JPanel mulPanel = new JPanel();
		mulPanel.setLayout(experimentLayout);
		mulPanel.add(avg_lineCountLabel);
		mulPanel.add(avg_lineCountLabelResult);		
		mulPanel.add(avg_blankLineCountLabel);
		mulPanel.add(avg_blankLineCountLabelResult);
		mulPanel.add(avg_spaceCountLabel);
		mulPanel.add(avg_spaceCountLabelResult);
		mulPanel.add(avg_wordCountLabel);
		mulPanel.add(avg_wordCountLabelResult);
		mulPanel.add(avg_charCountLabel);
		mulPanel.add(avg_charCountLabelResult);
		mulPanel.add(avg_averageCharacterLineLabel);
		mulPanel.add(avg_averageCharacterLineLabelResult);
		mulPanel.add(avg_averageWordLengthLabel);
		mulPanel.add(avg_averageWordLengthLabelResult);
		mulPanel.add(file_count);
		mulPanel.add(avg_file_count);
		add(mulPanel, BorderLayout.EAST);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == browseButton) {
			int returnVal = fileChooser.showOpenDialog(TextPanel.this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();
				
				//Create file parser and set appropriate label stats here
				fileHandler handle = new fileHandler();
				if(handle.fileValidation(file.getName())){
					statObject stats =  handle.fileStatistics(file.getAbsolutePath());
					this.results.add(stats);
					float new_line=0,new_blank=0,new_space=0,new_word=0,new_char=0,new_averageChar=0,new_averWord=0;
					for (int i = 0; i < this.results.size(); i++){
						statObject current = this.results.get(i);
						new_line = new_line + current.numericStats.get("lineCount");
						new_blank = new_blank + current.numericStats.get("blankLineCount");
						new_space = new_space + current.numericStats.get("spaceCount");
						new_word = new_word + current.numericStats.get("wordCount");
						new_char = new_char + current.numericStats.get("charCount");
						new_averageChar = new_averageChar + current.numericStats.get("averageCharLine");
						new_averWord = new_averWord+ current.numericStats.get("averageWordLen");
					}
					HashMap<String, Float> returnMap = new HashMap<String,Float>();
					HashMap<String, Integer> maxWordCounter = new HashMap<String,Integer>();
					returnMap.put("lineCount", (float)0);
					returnMap.put("blankLineCount", (float)0);
					returnMap.put("spaceCount", (float)0);
					returnMap.put("wordCount", (float)0);
					returnMap.put("charCount", (float)0);
					returnMap.put("averageCharLine", (float)0);
					returnMap.put("averageWordLen", (float)0);
					returnMap.put("maxWordOccur", (float)0);
					statObject new_stats = new statObject(returnMap, maxWordCounter, "");
					new_stats.numericStats.put("lineCount", new_line/this.results.size());
					new_stats.numericStats.put("blankLineCount",new_blank/this.results.size());
					new_stats.numericStats.put("spaceCount", new_space/this.results.size());
					new_stats.numericStats.put("worCount", new_word/this.results.size());
					new_stats.numericStats.put("charCount", new_char/this.results.size());
					new_stats.numericStats.put("averageCharLine", new_averageChar/this.results.size());
					new_stats.numericStats.put("averageWordLen", new_averWord/this.results.size());
					lineCountLabelResult.setText(Float.toString(stats.numericStats.get("lineCount")));
					blankLineCountLabelResult.setText(Float.toString(stats.numericStats.get("blankLineCount")));
					spaceCountLabelResult.setText(Float.toString(stats.numericStats.get("spaceCount")));
					wordCountLabelResult.setText(Float.toString(stats.numericStats.get("wordCount")));
					charCountLabelResult.setText(Float.toString(stats.numericStats.get("charCount")));;
					averageCharacterLineLabelResult.setText(Float.toString(stats.numericStats.get("averageCharLine")));;
					averageWordLengthLabelResult.setText(Float.toString(stats.numericStats.get("averageWordLen")));
					maxWordOccurResult.setText(Float.toString(stats.numericStats.get("maxWordOccur")));
					avg_lineCountLabelResult.setText(Float.toString(new_stats.numericStats.get("lineCount")));
					avg_blankLineCountLabelResult.setText(Float.toString(new_stats.numericStats.get("blankLineCount")));
					avg_spaceCountLabelResult.setText(Float.toString(new_stats.numericStats.get("spaceCount")));
					avg_wordCountLabelResult.setText(Float.toString(new_stats.numericStats.get("wordCount")));
					avg_charCountLabelResult.setText(Float.toString(new_stats.numericStats.get("charCount")));;
					avg_averageCharacterLineLabelResult.setText(Float.toString(new_stats.numericStats.get("averageCharLine")));;
					avg_averageWordLengthLabelResult.setText(Float.toString(new_stats.numericStats.get("averageWordLen")));
					avg_file_count.setText(Integer.toString(this.results.size()));
				}
			}
		}
	}

	private static void createAndShowGUI() {
		MyFrame frame = new MyFrame("T3XT");
		
		frame.add(new TextPanel());
		frame.setSize(1000, 1000);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		createAndShowGUI();
	}

}

class MyFrame extends JFrame {
	public MyFrame(String s) {
		setTitle(s);
		setSize(600, 300);
		setLocationRelativeTo(null);
		

		// Window Listeners
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	}
}
