import java.util.*;
import java.io.*;
import javax.swing.JOptionPane;

public class fileHandler {

    public  boolean fileValidation(String fileName){
        String[] checkTxt = fileName.split("\\.");
        if (checkTxt.length != 2){
            JOptionPane.showMessageDialog(null, "Filename incorrectly formatted.");
            return false;
        }
        else{
            if (checkTxt[1].equals("txt")){
                System.out.println("Filename correctly formatted.");
                return true;
            }
            else{
                JOptionPane.showMessageDialog(null, "Not a text file, please enter a valid text file.");
                return false;
            }
        }
    }
    public statObject fileStatistics(String fileName){
        String line = null;
        try{
            FileReader fileReader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            HashMap<String, Float> returnMap = new HashMap<String,Float>();
            HashMap<String, Integer> maxWordCounter = new HashMap<String,Integer>();
            returnMap.put("lineCount", (float)0);
            returnMap.put("blankLineCount", (float)0);
            returnMap.put("spaceCount", (float)0);
            returnMap.put("wordCount", (float)0);
            returnMap.put("charCount", (float)0);
            returnMap.put("averageCharLine", (float)0);
            returnMap.put("averageWordLen", (float)0);
            returnMap.put("maxWordOccur", (float)0);

            while ((line = bufferedReader.readLine()) != null){
                returnMap.put("lineCount", (returnMap.get("lineCount") + 1));
                if (line.length() == 0){
                    returnMap.put("blankLineCount", (returnMap.get("blankLineCount") + 1));
                }
                else{
                    String dummyLine = new String(line);
                    String charOnly = dummyLine.replace(" ", "");
                    returnMap.put("charCount",  returnMap.get("charCount") + charOnly.length());
                    String splitOnSpaces[] = line.split(" ");
                    returnMap.put("spaceCount", (returnMap.get("spaceCount") + splitOnSpaces.length - 1));
                    returnMap.put("wordCount", (returnMap.get("wordCount") + splitOnSpaces.length));
                    float tempCounter = 0, tempAverage = 0;
                    int tempMaxWord = 0;
                    for (int i = 0; i < splitOnSpaces.length; i++){
                        tempCounter = tempCounter + splitOnSpaces[i].length();
                        
                        if (maxWordCounter.keySet().contains(splitOnSpaces[i])){
                            tempMaxWord = maxWordCounter.get(splitOnSpaces[i]) + 1;
                            if (tempMaxWord > returnMap.get("maxWordOccur")){
                                returnMap.put("maxWordOccur", (float)tempMaxWord);
                            }
                            maxWordCounter.put(splitOnSpaces[i], tempMaxWord);
                        }

                        else{
                            maxWordCounter.put(splitOnSpaces[i], 1);
                            if (1 > returnMap.get("maxWordOccur")){
                                returnMap.put("maxWordOccur", (float)tempMaxWord);
                            }
                        }
                    }
                }

                
            }
            bufferedReader.close();
            returnMap.put("averageCharLine", returnMap.get("charCount") / returnMap.get("lineCount"));
            returnMap.put("averageWordLen", returnMap.get("charCount") / returnMap.get("wordCount"));
            statObject returnValue = new statObject(returnMap, maxWordCounter, fileName);
            return returnValue;

        }
        catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file '" + 
                fileName + "'");
            return new statObject();
        }
        catch(IOException ex) {
            System.out.println(
                "Error reading file '"
                + fileName + "'");                  
            ex.printStackTrace();
            return new statObject();
        }
    }
}